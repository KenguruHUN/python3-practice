#! /usr/bin/env python
"""Calculate tax."""

def tax(value, taxx):
    footpercent = (int(taxx) / 100) + 1
    grand_total = int(value) * footpercent

    return grand_total

value = input("Give me the price (without the tax): ")
taxx = input("Give me the tax: ")
print(tax(value, taxx))
