#! /usr/bin/env python
"""Python Statistics module test."""

import statistics

explame = [1,32,45,6,3,2,34,48,4,4,7,89,3,2,34,3,7,8,2,89,9,21,2,45,76,8,4,4,13,4,3,7,4,4,5]


# mean // átlag
v = statistics.mean(explame)
print(v)

# median // centrális érték (helyzeti középérték)
w = statistics.median(explame)
print(w)

# mode // leggyakoribb érték
x = statistics.mode(explame)
print(x)

# stdev //
y = statistics.stdev(explame)
print(y)

# variance //
z = statistics.variance(explame)
print(z)