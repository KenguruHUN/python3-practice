#! /usr/bin/env python
"""Find Cost of Tile to Cover W x L Floor."""


def flooring(length, width, cost):
    area = int(length) * int(width)
    flooring_cost = area * int(cost)
    return flooring_cost


print("Cost of flooring is %s whatever" % flooring(input("Length: "), input("Width: "), input("Cost: ")))

