#! /usr/bin/env python
"""Reverse a string."""


def str_rev(string):
    rev_string = string[::-1]
    return rev_string

print(str_rev(input("String: ")))
