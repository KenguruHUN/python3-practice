#! /usr/bin/env python
"""Fibonacci sequence."""

def fibo(number):
    a, b = 0, 1
    while a < number:
        print(a, end=' ')
        a, b = b, a + b


fibo(int(input("Give me a number: ")))