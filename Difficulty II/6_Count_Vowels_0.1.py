#! /usr/bin/env python
# -*- coding: UTF-8 -*-
"""Count vowels, consonants and spaces in a word or string."""


def count_vowels(str):
    vowels=('a','á','e','é','i','í','o','ó','ö','ő','u','ú','ü','ű')
    vcounter = 0
    ccounter = 0
    scounter = 0

    for c in str:
        if c == " ":
            scounter += 1
        else:
            for v in vowels:
                if c == v :
                    vcounter += 1
            else:
                ccounter += 1

    string = "There are %s vowels, %s consonants and %s spaces in '%s'." % (vcounter, ccounter-vcounter, scounter, str)
    return string


print(count_vowels(input("Give me a string: ")))