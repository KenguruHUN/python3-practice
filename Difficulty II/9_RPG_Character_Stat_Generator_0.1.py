#! /usr/bin/env python
"""RPG Charater Stat Generator"""

import random

class dice:
    """Roll one or more dice (e.g. 3d6 it means roll three six-sided dice)."""

    def __init__(self, string):
        string = string.split('d')
        self.list = []
        self.piece = int(string[0])
        self.side = int(string[1])

    def roll(self):

        if self.side == 10:
            if self.piece == 2:
                x = random.randrange(0, 9)
                y = random.randrange(0, 9)
                if x == 0 and y == 0:
                    z = 100
                else:
                    z = int(str(x) + str(y))
                self.list.append(z)
            elif self.piece > 2:
                for i in range(self.piece):
                    self.list.append(random.randrange(0,9))
            else:
                self.list.append(random.randrange(0,9))

            return self.list
        else:
            if self.piece > 1:
                for i in range(self.piece):
                    self.list.append(random.randrange(1, self.side+1))
            else:
                self.list.append(random.randrange(1, self.side+1))

            return self.list


a = dice('1d20')
print(a.roll())

b = dice('8d6')
print(b.roll())

d = dice('2d10')
print(d.roll())

