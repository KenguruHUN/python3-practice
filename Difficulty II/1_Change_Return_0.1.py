#! /usr/bin/env python
"""Simple change return calculator."""


def change_return(price, money):
    if money > price:
        return change(price,money)
    elif money < price:
        print("We need more %s money" % abs(money - price))
    elif price == money:
        print("Thank you for shopping!")
    else:
        print("Please give me more money!")


def change(price, money):
    change = price - money
    change = abs(change * 100)
    change = int(change)
    dollar_counter = 0
    half_counter = 0
    quater_counter = 0
    dime_counter = 0
    nickel_counter = 0
    penny_counter = 0
    while change > 0:
        if change % 100 == 0:
            change -= 100
            dollar_counter += 1
        elif change % 50 == 0:
            change -= 50
            half_counter += 1
        elif change % 25 == 0:
            change -= 25
            quater_counter += 1
        elif change % 10 == 0:
            change -= 10
            dime_counter += 1
        elif change % 5 == 0:
            change -= 5
            nickel_counter += 1
        elif change % 1 == 0:
            change -= 1
            penny_counter += 1

    print("we give you %s dollar(s), %s half(s), %s quater(s), %s dime(s), %s nickel(s) and %s penny(s)" % (dollar_counter, half_counter, quater_counter, dime_counter, nickel_counter, penny_counter))



change_return(float(input("Price: ")), float(input("Money: ")))