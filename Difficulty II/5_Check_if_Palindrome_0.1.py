#! /usr/bin/env python
"""Check if palindrome."""


def if_palindrome(str):
    nstr = str.replace(" ", "")
    rstr = nstr[::-1]

    if nstr == rstr:
        string = "The '%s' string is a palindrome" % str
        return string
    else:
        string = "Sorry the '%s' string is not a palindrome." % str
        return string


print(if_palindrome(input("Give me a string: ")))