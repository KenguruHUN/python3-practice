#! /usr/bin/env python
"""Algorithm of Euclid."""


def euclidae(*args):
    remain = True
    smaller = args[0]
    bigger = args[1]
    while remain:
        remain = bigger % smaller
        # print("Remain : ", remain)
        bigger = smaller
        # print("The biggest number now is : ", bigger)
        smaller = remain
        
    if remain == 0:
        print("Greatest common divisor : ", bigger)
    

def numerical(*args):
    for value in args:
        try:
            return int(value)
        except ValueError:
            return float(value)


def request(*args):
    bigger = 0
    smaller = 0

    if args[0] > args[1]:
        bigger = args[0]
        smaller = args[1]
    else:
        bigger = args[1]
        smaller = args[0]

    smaller = numerical(smaller)
    bigger = numerical(bigger)

    euclidae(smaller, bigger)

request(input("Num 1: "), input("Num 2: "))