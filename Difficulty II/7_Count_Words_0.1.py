#! /usr/bin/env python
# -*- coding: UTF-8 -*-

def count_words(str):
    splitStr = str.split(" ")
    wordsNum = len(splitStr)
    string = "The '%s' string contains %s words" % (str, wordsNum)
    return string


print(count_words(input("Give me a string: ")))