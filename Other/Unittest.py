#! /usr/bin/env python
"""First trying to implement a unittest."""

import unittest
#import datetime

def multiply(x, y):
    val = x*y
    return val


class multiplyTest(unittest.TestCase):


    # def setUp(self):
    #     pass

    def test_numbers_3_4(self):
        self.assertEqual(multiply(3,4), 12)

    def test_strings_a_3(self):
        self.assertEqual('a'*3, 'aaa')


def main():
    unittest.main()

if __name__ == '__main__':
    main()