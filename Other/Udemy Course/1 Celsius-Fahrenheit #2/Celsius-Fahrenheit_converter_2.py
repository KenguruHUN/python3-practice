#!/usr/bin/env python3
"""The first time in the course when we use if \o/ """

temperatures=[10,-20,-289,100]

def convert(temp):
    for t in temp:
        if t > (-273.15):
            fahr_temp = ((t*9)/5)+32
            print(fahr_temp)
        else:
            print("That temperature doesn't make sense!")


convert(temperatures)