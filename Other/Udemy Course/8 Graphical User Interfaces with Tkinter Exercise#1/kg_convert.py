#!/usr/bin/env python

from tkinter import *

window = Tk()

def kg_convert():
    gramms = float(e1_value.get())*1000
    pounds = float(e1_value.get())*2.20462
    ounces = float(e1_value.get())*35.274

    e2.insert(END, gramms)
    e3.insert(END, pounds)
    e4.insert(END, ounces)

l1 = Label(window, text="Kg")
l1.grid(row=0, column=0)

e1_value = StringVar()
e1 = Entry(window, textvariable=e1_value)
e1.grid(row=0, column=1)

b1 = Button(window, text="Convert", command=kg_convert)
b1.grid(row=0, column=2)

e2 = Entry(window)
e2.grid(row=1, column=0)

e3 = Entry(window)
e3.grid(row=1, column=1)

e4 = Entry(window)
e4.grid(row=1, column=2)

window.mainloop()