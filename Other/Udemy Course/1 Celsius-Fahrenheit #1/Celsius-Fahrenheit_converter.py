#!/usr/bin/env python3
"""Convert Celsius to Fahrenheit and vice versa (it's not a really good implementation of the course exercise, 'couse it's too overkill)"""

def convert(temp, unit):
    if unit == "C":
        fahr_temp = ((temp*9)/5)+32
        print("%s Fahrenheit" % fahr_temp)
    elif unit == "F":
        cels_temp = ((temp-32)*5)/9
        print("%s Celsius" % cels_temp)
    else:
        print("Please give me any unit (C as celsius or F as Fahrenheit)")

print("Enter the temperature first, after that enter the unit, C for celsius -> fahrenheit conversion, F for fahrenheit -> celsius conversion ")
temp = float(input("Temperature: "))
unit = input("Unit: ")
convert(temp, unit)