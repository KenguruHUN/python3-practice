#!/usr/bin/env python

import cv2,time

first_frame = None
status_list = []
video = cv2.VideoCapture(0)

while True:

    check, frame = video.read()
    status = 0
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #gray = cv2.GaussianBlur(gray, (21, 21), 10)


    if first_frame is None:
        first_frame = gray
        continue

    delta_frame = cv2.absdiff(first_frame, gray)
    thres_frame = cv2.threshold(delta_frame,30, 255, cv2.THRESH_BINARY)[1]
    thres_frame = cv2.dilate(thres_frame, None, iterations=2)

    (_,cnts,_) = cv2.findContours(thres_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in cnts:
        if cv2.contourArea(contour) < 10000:
            continue
        status = 1

        (x, y, w, h)= cv2.boundingRect(contour)
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0,255,0),3)

    status_list.append(status)

    cv2.imshow("Original", frame)
    #cv2.imshow("Capturing", gray)
    #cv2.imshow("Delta Frame", delta_frame)
    #cv2.imshow("Treshold Frame", thres_frame)


    key = cv2.waitKey(1)

    if key == ord('q'):
        break

print(status_list)

video.release()
cv2.destroyAllWindows()