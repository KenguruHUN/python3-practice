#!/usr/bin/env python

import cv2

face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
video = cv2.VideoCapture(0)

a = 0
while True:
    a += 1
    check, frame = video.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray,
                                          scaleFactor=1.2,
                                          minNeighbors=5)

    for x, y, w, h in faces:
        gray = cv2.rectangle(gray, (x, y), (x + w, y + h), (255, 255, 255), 3)

    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(gray, str(a), (10, 33), font, 1, (255,255,255), 2)

    cv2.imshow("Capturing", gray)


    key = cv2.waitKey(1)

    if key == ord('q'):
        break

video.release()
cv2.destroyAllWindows()
