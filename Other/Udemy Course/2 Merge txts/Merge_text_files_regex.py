#!/usr/bin/env python3

import os, datetime,re

container = os.listdir()
filename = datetime.datetime.now()
result = open(filename.strftime("%Y-%m-%d-%H-%M-%S-%f")+"_regex.txt", "w+")
for item in container:
    if re.search("^file\S\.txt", item):
        file = open(item, "r")
        result.write(file.read() + "\n")

        file.close()


result.close()