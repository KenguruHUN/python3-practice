#!/usr/bin/env python3

import os, datetime

container = os.listdir()
filename = datetime.datetime.now()
result = open(filename.strftime("%Y-%m-%d-%H-%M-%S-%f")+".txt", "w+")
for item in container:
    if ".txt" in item:
        file = open(item, "r")
        for line in file:
            result.write(line + "\n")

        file.close()

result.close()