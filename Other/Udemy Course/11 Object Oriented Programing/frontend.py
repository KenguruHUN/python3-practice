#!/usr/bin/env python
"""
A program that stores book information.
Title, Author
Year, ISBN

User can:
-View all record
-search an entry
-add entry
-update entry
-delete
-close
"""

from tkinter import *
from backend import Database

database = Database("books.db")

def get_selected_row(event):
    global selected_tuple
    index = list.curselection()[0]
    selected_tuple = list.get(index)
    entry_title.delete(0,END)
    entry_title.insert(END, selected_tuple[1])
    entry_author.delete(0,END)
    entry_author.insert(END, selected_tuple[2])
    entry_year.delete(0,END)
    entry_year.insert(END, selected_tuple[3])
    entry_isbn.delete(0,END)
    entry_isbn.insert(END, selected_tuple[4])

def view_command():
    list.delete(0, END)
    for row in database.view():
        list.insert(END, row)

def search_command():
    list.delete(0,END)
    for row in database.search(title_text.get(), author_text.get(), year_text.get(), isbn_text.get()):
        list.insert(END, row)

def add_command():
    database.insert(title_text.get(), author_text.get(), year_text.get(), isbn_text.get())
    list.delete(0, END)
    list.insert(END, (title_text.get(), author_text.get(), year_text.get(), isbn_text.get()))

def delete_command():
    database.delete(selected_tuple[0])

def update_command():
    database.update(selected_tuple[0], title_text.get(), author_text.get(), year_text.get(), isbn_text.get())

window = Tk()

window.wm_title("BookStore")

label1 = Label(window, text="Title")
label1.grid(row=0, column=0)

label2 = Label(window, text="Author")
label2.grid(row=0, column=2)

label3 = Label(window, text="Year")
label3.grid(row=1, column=0)

label4 = Label(window, text="ISBN")
label4.grid(row=1, column=2)

title_text = StringVar()
entry_title = Entry(window, textvariable=title_text)
entry_title.grid(row=0, column=1)

author_text = StringVar()
entry_author = Entry(window, textvariable=author_text)
entry_author.grid(row=0, column=3)

year_text = StringVar()
entry_year = Entry(window, textvariable=year_text)
entry_year.grid(row=1, column=1)

isbn_text = StringVar()
entry_isbn = Entry(window, textvariable=isbn_text)
entry_isbn.grid(row=1, column=3)

list = Listbox(window, height=7, width=35)
list.grid(row=2, column=0, rowspan=6, columnspan=2)

scroll_b = Scrollbar(window)
scroll_b.grid(row=2,column=2, rowspan=6)

list.configure(yscrollcommand=scroll_b.set)
scroll_b.configure(command=list.yview)

list.bind('<<ListboxSelect>>', get_selected_row)

view_button = Button(window, text="View All", width=12, command=view_command)
view_button.grid(row=2, column=3)

search_button = Button(window, text="Search Entry", width=12, command=search_command)
search_button.grid(row=3, column=3)

add_button = Button(window, text="Add Entry", width=12, command=add_command)
add_button.grid(row=4, column=3)

update_button = Button(window, text="Update", width=12, command=update_command)
update_button.grid(row=5, column=3)

delete_button = Button(window, text="Delete", width=12, command=delete_command)
delete_button.grid(row=6, column=3)

close_button = Button(window, text="Close", width=12, command=window.destroy)
close_button.grid(row=7, column=3)

window.mainloop()