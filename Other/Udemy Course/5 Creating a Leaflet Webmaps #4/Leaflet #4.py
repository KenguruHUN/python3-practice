#!/usr/bin/env python3

import folium
import pandas

df = pandas.read_csv("Volcanoes-USA.txt")

map = folium.Map(location=[45.372, -121.697], zoom_start=4, tiles='Stamen Terrain')

def color(elev):
    minimum = int(min(df['ELEV']))
    step = int((max(df['ELEV'])-min(df['ELEV']))/3)
    if elev in range(minimum, minimum+step):
        col = 'green'
    elif elev in range(minimum+step, minimum+step*2):
        col = 'orange'
    else:
        col = 'red'

    return col


for lat,lon, name in zip(df['LAT'], df['LON'], df['NAME']):
    map.add_child(folium.Marker(location=[lat, lon], popup=name, icon=folium.Icon(color=color(elev), icon_color='green')))

map.add_child(folium.GeoJson(data.open('convert.json'), name='World population'))

map.create_map(path='output5.html')