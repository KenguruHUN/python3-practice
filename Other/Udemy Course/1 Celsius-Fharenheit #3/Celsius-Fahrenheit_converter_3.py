#!/usr/bin/env python3

def convert(temp):
    file = open('temperatures.txt', 'w')

    for t in temp:
        if t > (-273.15):
            fahr_temp = ((t*9)/5)+32
            file.write("%s\n" % fahr_temp)
        else:
            print("That temperature doesn't make sense!")

    file.close()

temperatures=[10,-20,-289,100]
convert(temperatures)