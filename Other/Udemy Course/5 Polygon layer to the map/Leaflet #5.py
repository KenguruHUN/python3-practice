#!/usr/bin/env python3

import folium
import pandas

df = pandas.read_csv("Volcanoes-USA.txt")

map = folium.Map(location=[45.372, -121.697], zoom_start=4, tiles='Mapbox bright')

def color(elev):
    minimum = int(min(df['ELEV']))
    step = int((max(df['ELEV'])-min(df['ELEV']))/3)
    if elev in range(minimum, minimum+step):
        col = 'green'
    elif elev in range(minimum+step, minimum+step*2):
        col = 'orange'
    else:
        col = 'red'

    return col

fg = folium.FeatureGroup(name="Volcano Location")

for lat,lon, name, elev in zip(df['LAT'], df['LON'], df['NAME'], df['ELEV']):
    fg.add_child(folium.Marker(location=[lat, lon], popup=name, icon=folium.Icon(color=color(elev), icon_color='green')))

map.add_child(fg)

map.add_child(folium.GeoJson(data=open('convert.json'),
                             name='World population',
                             style_function=lambda x: {'fillColor':'green' if x['properties']['POP2005'] <= 10000000 else 'orange' if 10000000 < x['properties']['POP2005'] < 20000000 else 'red'}))

map.add_child(folium.LayerControl())

map.save(outfile='output5.html')