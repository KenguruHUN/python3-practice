#!/usr/bin/env python3

import folium

map = folium.Map(location==[45.372, -121.697], zoom_start=12)
map.create_map(path='output.html')