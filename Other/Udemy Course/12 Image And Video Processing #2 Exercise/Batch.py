#!/usr/bin/env python

import os, cv2

for file in os.listdir():
    if ".py" not in file:
        img = cv2.imread(file)
        resize_image = cv2.resize(img, (100, 100))
        cv2.imwrite("100x100_"+file, resize_image)